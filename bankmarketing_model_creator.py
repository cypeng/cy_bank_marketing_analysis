'''
@Author: CY Peng
@Date: 2019-08-25
@LastEditTime: 2019-11-7

DataVer  - Author - Note
2019/08/24    CY    First Release(20190824-R 1.0.0)
2019/11/7     CY    Using Model Helper
2019/12/02    CY    Add the Models Configeration
2019/12/20    CY    Data Preparation
'''

import pandas as pd
import os
import ModelSelector as MS
import SamplingLib as SM
import bclfModelCreatorHelper as MCH

import bankmarketing_preprocessing as bk_db

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    return data

def nested_training_stage(filename, database_folder, train_file_tail_tag = None,
                          features_engineering_model_path = os.path.join(".","EDA_output","train"), 
                          model_config_filename='models_config_test'):
    # Parameters Setting
    conf_matrix_params = {}
    conf_matrix_params['confusion_matrix'] = {}
    conf_matrix_params['confusion_matrix']['labels'] = [1, 0]
    conf_matrix_params['clf_report'] = {}
    conf_matrix_params['clf_report']['digits'] = 2

    #classifiers = custom_models_def()
    #grid_param = custom_models_param()
    classifiers, grid_param = MS.model_selector(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = rawData.loc[:, 'y'].to_frame()
    raw_x = rawData.drop(columns=['y'], axis=1)

    # Training Stage
    ModelHelper = MCH.MultiModelMLHelper(classifiers, grid_param)
    outer_fold_obj, inner_fold_obj = SM.two_stages_sampling_stratified_shuffle_split_obj()
    ModelHelper.data_preparation(raw_x, raw_y, train_file_tail_tag, features_engineering_model_path, bk_db.FeaturesEngineeringFit, LabelEncoderFitObj=bk_db.LabelEncoderFit) #,['poutcome']
    ModelHelper.nested_train(inner_fold_obj, outer_fold_obj, conf_matrix_params)
