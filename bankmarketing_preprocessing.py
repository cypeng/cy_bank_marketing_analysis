'''
@Author: CY Peng
@Date: 2019-08-25
@LastEditTime: 2019-11-5

DataVer  - Author - Note
2019/08/24    CY    First Release(20190824-R 1.0.0)
2019/11/1     CY    ML Lib New Function Included
2019/11/5     CY    Information Update
2019/11/18    CY    Numerical/ Categorical Data PCA
2019/11/22    CY    Basic Features Engineering
2019/11/27    CY    ML Lib New Function Included
2019/12/20    CY    Data Preparation

Reference
https://www.kaggle.com/yufengsui/machine-learning-project-bank-marketing-analytics/notebook

'''
# Common Lib
#import kaggle_login as kaggle
import sys
import os
import pandas as pd
import CommonUtility as CU
import matplotlib.pyplot as plt
import re
import numpy as np
import pickle

# Constom ML Lib
import ExploratoryDataAnalysis as EDA
import LearningAnalysis as LA
import PreprocessingUtility as PU
import SamplingLib as SM
import ModelEvaluation as ME

outputFolder = "EDA_output"
CU.createOutputFolder(outputFolder)

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    return data

def EDABasicProfile(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    EDA.df_data_check(data, outputFolder, filename)
    EDA.df_data_type_check(data, outputFolder, filename)

def EDAComponentsAnalysis(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))
    pcaObj = LA.PrincipleComponentAnalysis(df_data, outputFolder, filename)
    LA.VisualizingN2MEigenVector(pcaObj, df_data, (0, 5), outputFolder, filename)

def LabelEncoderFit(filename, database_folder, model_file_path):
    df_data = read_data(os.path.join(database_folder, filename))
    df_data['y'] = df_data['y'].map({'no': 0, 'yes': 1}).astype(int)
    df_data.to_csv(os.path.join(database_folder, filename+'.csv'), index=False)

def FeaturesEngineering(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))
    numerical_data_type = ['age', 'balance', 'day', 'duration', 'campaign', 'pdays', 'previous']
    categorical_data_type = ['job', 'marital', 'education', 'contact', 'month', 'poutcome']
    binary_data_type = ['housing', 'default', 'loan']
    label_data_type = ['y']

    # Categorical Data Type: One Hot Encoder + Imputation
    oheObj = PU.cat_one_hot_encoder(df_data.loc[:, categorical_data_type], outputFolder, filename, sparse=False)
    ohe_cat_data = oheObj.fit_transform(df_data.loc[:, categorical_data_type])
    ohe_cat_data = pd.DataFrame(ohe_cat_data, columns=oheObj.get_feature_names())

    ohe_cat_data = PU.convert_missing_ohe_to_nan(ohe_cat_data, oheObj.get_feature_names(), missing_name='unknown')
    imp_ohe_cat_data = PU.num_fastKNN(ohe_cat_data, 3000, outputFolder, filename, k =30, **{})
    imp_ohe_cat_data.columns = ohe_cat_data.columns

    # Binary Data Type: Data Mapping
    df_data = bi_data_map(df_data, binary_data_type)
    df_data = bi_data_map(df_data, label_data_type)

    # Numerical Data Type: Standardization, Normalization
    stdDataObj = PU.num_standardization(df_data.loc[:, numerical_data_type], outputFolder, filename)
    std_num_data = stdDataObj.transform(df_data.loc[:, numerical_data_type])
    std_num_data = pd.DataFrame(std_num_data, columns=numerical_data_type)

    norDataObj = PU.num_normalizer(df_data.loc[:, numerical_data_type], outputFolder, filename, **{})
    nor_num_data = norDataObj.transform(df_data.loc[:, numerical_data_type])
    nor_num_data = pd.DataFrame(nor_num_data, columns=numerical_data_type)

    # Data Storing
    num_ohe = pd.concat([df_data.loc[:, numerical_data_type], imp_ohe_cat_data, df_data.loc[:, binary_data_type], df_data.loc[:, label_data_type]], axis=1)
    num_ohe.to_csv(os.path.join(database_folder, filename + "_num_ohe_bi.csv"), index=False)

    std_ohe = pd.concat([std_num_data, imp_ohe_cat_data, df_data.loc[:, binary_data_type], df_data.loc[:, label_data_type]], axis=1)
    std_ohe.to_csv(os.path.join(database_folder, filename + "_std_ohe_bi.csv"), index=False)

    nor_ohe = pd.concat([nor_num_data, imp_ohe_cat_data, df_data.loc[:, binary_data_type], df_data.loc[:, label_data_type]], axis=1)
    nor_ohe.to_csv(os.path.join(database_folder, filename + "_nor_ohe_bi.csv"), index=False)

def FeaturesEngineeringFit(filename, database_folder, model_file_path):
    df_data = read_data(os.path.join(database_folder, filename))
    numerical_data_type = ['age', 'balance', 'day', 'duration', 'campaign', 'pdays', 'previous']
    categorical_data_type = ['job', 'marital', 'education', 'contact', 'month', 'poutcome']
    binary_data_type = ['housing', 'default', 'loan']
    label_data_type = ['y']

    # Categorical Data Type: One Hot Encoder + Imputation
    with open(model_file_path + "_ohe.sav", 'rb') as file:
         oheObj = pickle.load(file)
    #oheObj = PU.cat_one_hot_encoder(df_data.loc[:, categorical_data_type], outputFolder, filename, sparse=False)
    ohe_cat_data = oheObj.fit_transform(df_data.loc[:, categorical_data_type])
    ohe_cat_data = pd.DataFrame(ohe_cat_data, columns=oheObj.get_feature_names())

    ohe_cat_data = PU.convert_missing_ohe_to_nan(ohe_cat_data, oheObj.get_feature_names(), missing_name='unknown')
    try:      
        imp_ohe_cat_data = PU.num_fastKNN(ohe_cat_data, 3000, outputFolder, filename, k =30, **{})
        imp_ohe_cat_data.columns = ohe_cat_data.columns
    except:
        imp_ohe_cat_data = ohe_cat_data

    # Binary Data Type: Data Mapping
    df_data = bi_data_map(df_data, binary_data_type)
    df_data = bi_data_map(df_data, label_data_type)

    # Numerical Data Type: Standardization, Normalization
    with open(model_file_path + "_std.sav", 'rb') as file:
         stdDataObj = pickle.load(file)
    #stdDataObj = PU.num_standardization(df_data.loc[:, numerical_data_type], outputFolder, filename)
    std_num_data = stdDataObj.transform(df_data.loc[:, numerical_data_type])
    std_num_data = pd.DataFrame(std_num_data, columns=numerical_data_type)

    with open(model_file_path + "_nor.sav", 'rb') as file:
         norDataObj = pickle.load(file)
    #norDataObj = PU.num_normalizer(df_data.loc[:, numerical_data_type], outputFolder, filename, **{})
    nor_num_data = norDataObj.transform(df_data.loc[:, numerical_data_type])
    nor_num_data = pd.DataFrame(nor_num_data, columns=numerical_data_type)

    # Data Storing
    num_ohe = pd.concat([df_data.loc[:, numerical_data_type], imp_ohe_cat_data, df_data.loc[:, binary_data_type]], axis=1)
    num_ohe.to_csv(os.path.join(database_folder, filename + "_num_ohe_bi.csv"), index=False)

    std_ohe = pd.concat([std_num_data, imp_ohe_cat_data, df_data.loc[:, binary_data_type]], axis=1)
    std_ohe.to_csv(os.path.join(database_folder, filename + "_std_ohe_bi.csv"), index=False)

    nor_ohe = pd.concat([nor_num_data, imp_ohe_cat_data, df_data.loc[:, binary_data_type]], axis=1)
    nor_ohe.to_csv(os.path.join(database_folder, filename + "_nor_ohe_bi.csv"), index=False)    

def EDADTDumpROC(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    y = data.loc[:, 'y'].to_frame()
    X = data.drop(columns=['y'], axis=1)
    ME.df_data_features_ranking_roc_curve(X, y, outputFolder, filename)

def df_data_numerical_data_distribution_plot_check(df_data, check_tag, raw_num_data_type, num2cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Distribution', ylabel_list = ["No. of Client(s)"], fig_size = fig_size)
    #EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), num2cat_data_type, None, outputFolder, filename+check_tag, #plot_type = 'Count', ylabel_list = ["No. of Client(s)"], fig_size = fig_size)

def df_data_categorical_data_distribution_plot_check(df_data, check_tag, raw_cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Count', ylabel_list = ["No. of Client(s)"], fig_size = fig_size)

def EDABasicStatistics(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))
    raw_num_data_type = ['age', 'balance', 'day', 'duration', 'campaign', 'pdays', 'previous']
    raw_cat_data_type = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'poutcome']
    num2cat_data_type = [None]
    # binary_data_type = ['default', 'housing', 'loan']
    label_data_type = ['y']

    # Data Check
    df_data_numerical_data_distribution_plot_check(df_data.dropna(), '_basic', raw_num_data_type, num2cat_data_type, filename, (10, 10))
    df_data_categorical_data_distribution_plot_check(df_data, '_basic', raw_cat_data_type, filename, (10, 10))

    # Numerical Data Type
    EDA.df_data_ANOVA_test(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    EDA.df_data_numerical_type_describe(df_data, outputFolder, filename) #.fillna(value=-1)
    #EDA.df_data_multi_type_plot(df_data.fillna(value=-1), numerical_data_type, None, outputFolder, filename, 
    #                            plot_type = 'Distribution', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    EDA.df_data_numerical_type_correlation_heatmap(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    
    #EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), bi_categorical_data_type, None, outputFolder, filename+'_single', plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    
    # All Data Type
    EDA.df_data_percentage_description(df_data.fillna(value="unknown"), None, outputFolder, filename)

def EDABasicStatisticsLabeledCorr(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))
    raw_num_data_type = ['age', 'balance', 'day', 'duration', 'campaign', 'pdays', 'previous']
    raw_cat_data_type = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'poutcome']
    num2cat_data_type = [None]
    numerical_data_type_ylabel = ["No. of Client(s)"]
    # binary_data_type = ['default', 'housing', 'loan']
    label_data_type = ['y']

    # Preprocessing
    #df_data = num_basic_processing(df_data)
    #df_data = cat_basic_processing(df_data)
    #raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, #label_data_type, drop_out_data_type = eda_basic_statistics_data_type_def()
	
	# Numerical Data Type
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, label_data_type[0], outputFolder, filename, 
                                plot_type = 'Box', ylabel_list = numerical_data_type_ylabel, fig_size = (10, 10))
    EDA.df_data_numerical_type_pairscatter_plot(df_data.fillna(value=-1), raw_num_data_type, label_data_type[0],
                                                outputFolder, filename)

    # Categorical Data Type
    EDA.df_data_percentage_description(df_data.fillna(value="unknown"), label_data_type[0], outputFolder, filename)
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, label_data_type[0], outputFolder, filename,plot_type = 'Count', ylabel_list = ["No. of Client(s)"], fig_size = (10, 10))     
    EDA.df_data_multi_type_percentage_value_bar_plot(df_data.fillna(value="unknown"), raw_cat_data_type,
                                                     label_data_type[0], outputFolder, filename, ylabel_list = None)

def cat_unknown_replace_nan(df_data, missing_name):
    df_data = df_data.replace(missing_name, np.nan)
    return df_data

def bi_data_map(df_data, type_list):
    for binary_type in type_list:
        df_data.loc[:, binary_type] = df_data.loc[:, binary_type].map({'no': 0, 'yes': 1}).astype(int)
    #df_data.loc[:, label_data_type] = df_data.loc[:, label_data_type].map({'no': 0, 'yes': 1}).astype(int)
    return df_data

def ImbalanceRawDataPrepraption(filename, database_folder):
    # Data Preparation
    kwargs = {}
    rawData = read_data(os.path.join(database_folder, filename))
    #training_data = bankmarketing_db.FeaturesEngineering(rawData)
    y = rawData.loc[:, 'y'].to_frame()
    X = rawData.drop(columns=['y'], axis=1)
    # x_nested, y_nested = SM.imb_data_SMOTEsampling(X, y, method='general', **kwargs)
    x_nested, y_nested = SM.imb_data_RandomUnderSampler(X, y, **kwargs)
    print('After OverSampling, the shape of X: {}'.format(x_nested.shape))
    print('After OverSampling, the shape of y: {}'.format(y_nested.shape))
    print("After OverSampling, counts of label '1': {}".format(sum(np.array(y_nested) == 1)))
    print("After OverSampling, counts of label '0': {}".format(sum(np.array(y_nested) == 0)))
    imb_raw_data = pd.concat([x_nested, y_nested], axis=1)
    imb_raw_data.to_csv(os.path.join(database_folder, filename + "_imb.csv"), index=False)

def cat_mapping_visualization(df_data):
    df_data['job'] = df_data['job'].map({'admin.': 0, 'unemployed': 1, 'management': 2,
                                         'housemaid': 3, 'entrepreneur': 4, 'student': 5,
                                         'blue-collar': 6, 'self-employed': 7, 'retired': 8,
                                         'technician': 9, 'services': 10, 'unknown': -1}).astype(int)
    df_data['marital'] = df_data['marital'].map({'single': 0, 'divorced': 1, 'married': 2}).astype(int)
    df_data['education'] = df_data['education'].map(
        {'unknown': -1, 'secondary': 0, 'primary': 1, 'tertiary': 2}).astype(int)
    df_data['housing'] = df_data['housing'].map({'no': 0, 'yes': 1}).astype(int)
    df_data['default'] = df_data['default'].map({'no': 0, 'yes': 1}).astype(int)
    df_data['loan'] = df_data['loan'].map({'no': 0, 'yes': 1}).astype(int)
    df_data['contact'] = df_data['contact'].map({'unknown': -1, 'telephone': 0, 'cellular': 1}).astype(int)
    df_data['month'] = df_data['month'].map(
        {'jan': 1, 'feb': 2, 'mar': 3, 'apr': 4, 'may': 5, 'jun': 6,
         'jul': 7, 'aug': 8, 'sep': 9, 'oct': 10, 'nov': 11, 'dec': 12}).astype(int)
    df_data['poutcome'] = df_data['poutcome'].map({'unknown': -1, 'failure': 0, 'other': 1, 'success': 2}).astype(int)
    df_data['y'] = df_data['y'].map({'no': 0, 'yes': 1}).astype(int)
    return df_data

def EDA_outlier_percentage_check(df_data):
    return df_data

