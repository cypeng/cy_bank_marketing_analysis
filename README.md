# Case Study: Bank Marketing Analysis for Imbalance Data, UCI Machine Learning Repository
<!-- blank line -->
<dl>
　<dd>C.Y. Peng</dd>
<!-- blank line -->
　<dd>Keywords: Data Science, Exploratory Data Analysis, Feature Engineering, Principle Component Analysis, Machine Learning</dd> 
</dl> 


## Lib Information
- Release Ver: 20200119-R 1.0.0
- Lib Ver: 20200119
- Author: C.Y. Peng
- Required Lib: requirements_venv36dl.txt
- OS Required: Windows 64 bit
- Data Source: [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php) - Bank Marketing Data Set (Portuguese banking institution)​
- This project differ from the reference [1]. Bank marketing analysis for balance data research in reference 1. 

## Part I. Overview.
This project, the main topic of the implementation of bank marketing analysis by using the data science methodology.
We cover several segments as follows:
- [x] Project Overview
- [x] Fundemental Principle
   - [x] Introduction to the Data Science
   - [x] Bank Marketing Analysis Overview
- [x] Data Analysis through the Data Science  
   - [x] From EDA to Model Established
   - [x] Analysis Conclusion
- [x] Quick Start
   - [x] Project Lib Architecture
   - [x] Model Established Pipeline
   - [x] Operation Mode
- [x] Other Records
- [x] Reference

Data science is a technology to extract the effective information from the large data. This repo enables you to have a quick understanding of the data science from bank marketing analysis.

## Part II. Fundemental Principle
### Introduction to the Data Science  
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/DataScienceStudyFlow.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 1. Data Science Flow Chart
</div>
<!-- blank line -->

Data science is a multi-disciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from structured and unstructured data.
There are five steps to established the prediction system from the large data, as Figure 1:
- [1] Problem Definition:  Using the professional view to define the problem and the objective of the solving of the problem
- [2] Exploratory Data Analysis, EDA: First, we use the statistics methods to analyze the data sets to summerize the main characteristics from data collection and data clean. Second, we use the principle component analysis to analyze the features importance and the features selection.
- [3] Feature Engineering: Converting the raw data to the features and using these features to established the model.
- [4] Model Established: Spliting the data to the training, the testing and the validation data to established the model, select the model and optimize the model. 
- [5] Model Maintenance: Runtime model implementation, monitoring, and the diagnosis.

### Bank Marketing Analysis Overview [1]
There are two main approaches for enterprises to promote products: through mass campaigns, targeting general indiscriminate public or directed marketing, targeting a specific set of contacts (Ling and Li 1998). Nowadays, in a global competitive world, positive responses to mass campaigns are typically very low, irected marketing focus on targets that assumable will be keener to that specific products, making this kind of campaigns more attractive due to its efficiency. 
Therefore, in this project, we fucus on the improvement in campagin efficiency; also, the same drivers are pressing for a reduction in costs and time. Lesser contacts should be done, but an approximately number of successes (clients subscribing the deposit) should be kept.

There are some tips for this project as following:
- Data Source: [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php) - Bank Marketing Data Set (Portuguese banking institution)​
- For this project, we select the full dataset: 17 variables​, between May 2008 and November 2010.
- The marketing campaigns were based on phone calls. Often, more than one contact to the same client was required, in order to access if the product (bank term deposit) would be ('yes') or not ('no') subscribed. ​

### Problem Definition  
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/BankMarketingAnalysisDefine.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 2. Bank Marketing Analysis Problem Definition
</div>
<!-- blank line -->

According to the reference [1], we could reduce the problem to the math problem:
- The data sets are 45211 data​, which including the one variable for the output​.
- We want to established the binary classification system to predict the client required if the product order (yes or no).
- This project show the analysis for bank marketing.

## Data Analysis through the Data Science:
### From EDA to Model Established
#### Bank Marketing Data Basic Profile
​First, we look into the insights for the all data as following as tables, there are three investigations in this data sets: 

    | No. | Variables | Data Type            | Unique Data  | Description                                                                                                                        |
    |-----|-----------|----------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------|	
    | 1   | age       | numeric, integer     |              |                                                                                                                                    |
    | 2   | job       | categorical, nominal | 12           | admin, unknown, unemployed, management, housemaid, entrepreneur, student, blue-collar, selfemployed, retired, technician, services |
    | 3   | marital   | categorical, nominal | 3            | married, divorced, single​                                                                                                          |
    | 4   | education | categorical, nominal | 4            | unknown, secondary, primary, tertiary                                                                                              |
<div align="center">

Table 1. Client Background Information
</div>
<!-- blank line -->                                    
     
	| No. | Variables | Data Type                | Unique Data |  Description                     |
	|-----|-----------|--------------------------|-------------|----------------------------------|
	| 1   | default   | categorical, binary      | 2           | has credit in default?           |
    | 2   | balance   | numeric, integer         |             | average yearly balance, in euros |
    | 3   | housing   | categorical, binary      | 2           | has housing loan?                |
	| 4   | loan      | categorical, binary      | 2           | has personal loan?               |
<div align="center">

Table 2. Client-Bank Information
</div>
<!-- blank line --> 

     
    | No. | Variables | Data Type            | Unique Data |  Description                                                                                | 
    |-----|-----------|----------------------|-------------|---------------------------------------------------------------------------------------------| 	 
	| 1   | contact   | categorical, nominal | 3           | contact communication type​                                                                  |
	|     |           |                      |             | unknown, telephone, cellular                                                                |
	| 2   | day       | numeric, integer     |             | last contact day of the month                                                               |
	| 3   | month     | categorical, nominal | 12          | last contact month of year​ jan, feb, mar, ..., nov, dec                                     |
	| 4   | duration  | numeric, integer     |             | last contact duration, in seconds                                                           |
	| 5   | campaign  | numeric, integer     |             | number of contacts performed during this campaign and for this client                       |
	| 6   | pdays     | numeric, integer     |             | number of days that passed by after the client was last contacted from a previous campaign​  |
	|     |           |                      |             | -1: client was not previously contacted                                                     |
	| 7   | previous  | numeric, integer     |             | number of contacts performed before this campaign and for this client​                       |
	| 8   | poutcome  | categorical, nominal | 4           | outcome of the previous marketing campaign​                                                  |
    |     |           |                      |             | unknown, other, failure, success​                                                            |
<div align="center">

Table 3. Compaign Information
</div>
<!-- blank line --> 
														 
From the Table 1.-Table 3., some unknown data in the categorical data, these data view as the missing data.														 
										
Second, we look into the numerical data, we organize the statistics of the numerical data as following as the tables and the figures:  
  														                                
    | Statistics | age   | balance | day   | duration | campaign | pdays  | previous | 
    |------------|-------|---------|-------|----------|----------|--------|----------| 			
    | mean       | 40.94 | 1362.27 | 15.81 | 258.16   | 2.76     | 40.20  | 0.58     |
    | std        | 10.62 | 3044.77 | 8.32  | 257.53   | 3.10     | 100.13 | 2.30     |
    | min        | 18    | -8019   | 1     | 0        | 1        | -1     | 0        |
    | 25%        | 33    | 72      | 8     | 103      | 1        | -1     | 0        |
    | 50%        | 39    | 448     | 16    | 180      | 2        | -1     | 0        |
    | 75%        | 48    | 1428    | 21    | 319      | 3        | -1     | 0        |
    | max        | 95    | 102127  | 31    | 4918     | 63       | 871    | 275      |
<div align="center">

Table 4. Numerical Data Statistic Description
</div>
<!-- blank line --> 
                                                                                         
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_basic_multi_type_versus_None_Distribution.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 3. Numerical Data Distribution Plot
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_multi_type_versus_y_Box.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 4. Numerical Data Versus Output Box Plot
</div>
<!-- blank line -->
 
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_pairplot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 5. Numerical Data Versus Output Pair Scatter Plot
</div>
<!-- blank line -->
  
From above Table 4. and Figure 3.-5., we find that the:
- age: 
  - All clients are over 18 years old, the range of the age of the client who required product order are more wide than not required (Table 4., Figure 3.-4.).
  - Over than 60 years old, clients who required product order are more than clients who are not required (Figure 5.).
- balance: 
  - The balance of the some clients are negative about 15%-20% of all clients (Table 4., Figure 3.-4.).
  - Most clients of good balance who are higher possibility of required product order (Figure 3.-5.). 
- day:
  - Most clients who required order are last contact day over around 8 days (Table 4., Figure 3.-4.).
  - The clients, last contact about around 10 days or 20 days, highly possible required product order (Figure 5.). 
- duration:
  - Most (70-80%) clients for last contact duration are under 5 minutes; clients who have been in contact for a longer period of time are more likely to required product order (Table 4., Figure 3.-5.).
- campaign:
  - Most (70-80%) clients for contact number of this compaign are under 5 times; clients who required to product order do not need to contact more often (Table 4., Figure 3.-5.).
- pdays: 
  - The percentage of the missing data is too high, over 75% of the pdays are -1. Less information in the pdays variable (Table 4.). 
  - Clients who need to order goods, number of days that passed by after the client was last contacted may not be long (Figure 3.-5.).
- previous: 
  - The number of contacts performed before this campaign and for most clients are 0 time (Table 4., Figure 3.-4.).​
  - Too many times of contacts performed before this campaign and for clients who possible don't required product order (Figure 5.). 
	
We could drived the conbination from the correlation map, all these data correlation as following as figures: 
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_linear_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 6. Linear Correlation Heat Map Plot
</div>
<!-- blank line -->
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_kendall_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 7. Kendall Correlation Heat Map Plot
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_pearson_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 8. Pearson Correlation Heat Map Plot
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_spearman_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 9. Spearman Correlation Heat Map Plot
</div>
<!-- blank line -->

Only pdays and poutcome are highly correlation but other data are not outstanding phenomenon as Figure 6. - Figure 9.

Furthermore, we show the categorical data count plot as following as figures:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_basic_multi_type_versus_None_Count.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 10. Categorical Data Count Plot
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_multi_type_versus_y_Count.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 11. Categorical Data Versus Output Count Plot
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_per_bar.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 12. Categorical Data Versus Output Percentage Plot
</div>
<!-- blank line -->
 
From above figures, we look into the categorical data, we show the statistics of the categorical data as following as the tables:
                                                                        
    |               | job   |          | marital |           | education |           | contact |     | month |          | poutcome | 
    |---------------|-------|----------|---------|-----------|-----------|-----------|---------|-----|-------|----------|----------|
	| blue-collar   | 0.22  | married  | 0.60    | secondary | 0.51      | cellular  | 0.65    | may | 0.30  | success  | 0.03     |
    | management    | 0.21  | single   | 0.28    | tertiary  | 0.29      | telephone | 0.06    | jul | 0.15  | failure  | 0.11     |
    | technician    | 0.17  | divorced | 0.12    | primary   | 0.15      | unknown   | 0.29    | aug | 0.14  | other    | 0.04     |
    | admin.        | 0.11  | unknown  |         | unknown   | 0.04      |           |         | jun | 0.12  | unknown  | 0.82     |
    | services      | 0.09  |          |         |           |           |           |         | nov | 0.09  |          |          |
    | retired       | 0.05  |          |         |           |           |           |         | apr | 0.06  |          |          |
    | self-employed | 0.03  |          |         |           |           |           |         | feb | 0.06  |          |          |
    | entrepreneur  | 0.03  |          |         |           |           |           |         | jan | 0.03  |          |          |
    | unemployed    | 0.03  |          |         |           |           |           |         | oct | 0.02  |          |          |
    | housemaid     | 0.27  |          |         |           |           |           |         | sep | 0.01  |          |          |
    | student       | 0.21  |          |         |           |           |           |         | mar | 0.01  |          |          |
    | unknown       | 0.01  |          |         |           |           |           |         | dec | 0.01  |          |          | 
<div align="center">

Table 5. Percentage of the Each Data Type in Categorical Data
</div>
<!-- blank line -->
                                                                        
    |output | job   | marital | education | default | housing | loan | contact | month | poutcome |
    |-------|-------|---------|-----------|---------|---------|------|---------|-------|----------|
    | yes   | 0.13  | 0.12    | 0.12      | 0.09    | 0.12    | 0.10 | 0.11    | 0.24  | 0.26     |   
	| no    | 0.87  | 0.88    | 0.88      | 0.91    | 0.88    | 0.90 | 0.89    | 0.76  | 0.74     |

<div align="center">

Table 6. Percentage of the Categorical Data versus Output
</div>
<!-- blank line --> 

    |     | default | housing | loan |
    |-----|---------|---------|------|
    | yes | 0.02    | 0.56    | 0.16 | 
    | no  | 0.98    | 0.44    | 0.84 |

<div align="center">

Table 7. Percentage of the Positive/ Negative Sampling Categorical Data
</div>
<!-- blank line --> 
                                                                        
According to above the tables, the classification problem is the imbalance data binary classification problem.
                                                                        
From above tables and figures, we understand that:
- job: 
  - Low unknown percentage, above 0 and under 0.1; imbalance sampling for each type for job variable (Table 5., Figure 10.-12.).
  - Client, who is a student or retired, possible required product order (Table 6., Figure 11.-12.).
- marital: 
  - There is no unknown in the marital (Table 5., Figure 10.).
  - Client, who is single or divorced, possible required product order (Table 6., Figure 11.-12.).
- education: 
  - Low unknown percentage, above 0 and under 0.1; most clients, who are unknown education degree, don't required product order (Table 5., Figure 10.-12.). 
  - Client whose good education possible required product order (Table 6., Figure 11.-12.). 
- contact: 
  - Unknown percentage is close to 0.30 (Table 5., Figure 10.-12.).
  - There are similar percentage for client who required product order in the contact variable (Table 6., Figure 11.-12.). 
- month:
  - For month variable, most sampling data in May (Table 5., Figure 10.).
  - Clients highly possible required product order in Mar, September, October or December (Table 6., Figure 11.-12.).
- poutcome: 
  - High unknown percentage, 0.82. We will remove this variable from the data set to established the model in the future [2] (Table 5., Figure 10.-12.). 
  - High percentage success if the last marketing campaign success (Table 6., Figure 11.-12.). 
- default:
  - Most clients have no credit default (Table 7., Figure 10.).
  - Maybe clients for no credit default possible required product order (Table 6., Figure 11.-12.). 
- housing:
  - Balance sampling for housing variable (Table 7., Figure 10.).
  - Imbalance sampling for housing variable versus output (Table 6., Figure 11.-12.)
  - Maybe clients for no housing loan possible required product order (Table 6., Figure 11.-12.). 
- loan:
  - Most clients have no personal loan (Table 7., Figure 10.).
  - Maybe clients for no personal loan possible required product order (Table 6., Figure 11.-12.). 
- Overall:
  - Most of the samples are imbalance in categorical type (Figuare 11.-12.).
- Missing Data: Unknown for Categorical​ Data Type
<!-- blank line --> 

#### Imbalance Data Binary Classification Problem Handling
Based on the last section, we define the bank marketing problem as the imbalance data binary classification, output yes/no percentage are about 1:9. 
In bank marketing binary classification, we heavily focus on the positive ouput in the business overview; therefore, we need to takle the data by downsampling. 
Usually, there are two methods to handling the imbalance data for the classification, oversampling and undersampling. 
Oversampling refers to various methods that aim to increase the number of instances from the underrepresented class in the data set.
Most classification algorithms will heavily focus on the majority class by using the oversampling; but in this case, we heavily fucus on the minority class, positive output.
There are several other ways to attack minority, such as undersampling or combinations of the two. 
In this project, we use the simple undersampling technology to takle the imbalance problem, random downsampling.

This section, we make a comprehensive survey about the EDA after data downsampling, there are 10578 data in the set.
First, We show the statistics of the numerical data as following as table:  
  														                                
    | Statistics | age   | balance | day   | duration | campaign | pdays  | previous | 
    |------------|-------|---------|-------|----------|----------|--------|----------| 			
    | mean       | 41.31 | 1530.91 | 15.54 | 379.12	  | 2.51	 | 52.55  | 0.85     |
    | std        | 11.99 | 3218.29 | 8.3   | 349.84   |	2.73     | 108.80 |	2.29     |
    | min        | 18    | -3372   | 1     | 0        | 1        | -1     | 0        |
    | 25%        | 32    | 117     | 8     | 144      | 1        | -1     | 0        |
    | 50%        | 39    | 547     | 15    | 260      | 2        | -1     | 0        |
    | 75%        | 49    | 1728    | 21    | 506      | 3        | 66     | 1        |
    | max        | 95    | 81204   | 31    | 3881     | 44       | 854    | 58       |
                
<div align="center">

Table 8. Numerical Data Statistic Description for Undersampling Data
</div>
<!-- blank line --> 
				
These numerical data distribution as following as figure: 
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_basic_multi_type_versus_None_Distribution.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 13. Numerical Data Distribution Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_multi_type_versus_y_Box.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 14. Numerical Data Versus Output Box Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_pairplot.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 15. Numerical Data Versus Output Pair Scatter Plot for Undersampling Data
</div>
<!-- blank line -->
  
From Figure 13.-15., similar to the last section, we discover this distribution is very close to the sampling population, and the binary output percentage are very similar.
	
Again, these data correlation as following as figures:  
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_linear_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 16. Linear Correlation Heat Map Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_kendall_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 17. Kendall Correlation Heat Map Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_pearson_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 18. Pearson Correlation Heat Map Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_spearman_correlation_map.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 19. Spearman Correlation Heat Map Plot for Undersampling Data
</div>
<!-- blank line -->
 
The same results, we could drived the conbination from the correlation map, only pdays and poutcome are highly correlation but other data are not outstanding correlation as Figure 16. - Figure 19.

Next, we show the statistics of the categorical data as following as the tables and figures:

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_basic_multi_type_versus_None_Count.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 20. Categorical Data Count Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_multi_type_versus_y_Count.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 21. Categorical Data Versus Output Count Plot for Undersampling Data
</div>
<!-- blank line -->

<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_per_bar.svg" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 22. Categorical Data Versus Output Percentage Plot for Undersampling Data
</div>
<!-- blank line -->

From above figures, we will summarize the the percentage of all categorical data type contents:
                                                                             
    |               | job   |          | marital |           | education |           | contact |     | month |          | poutcome | 
    |---------------|-------|----------|---------|-----------|-----------|-----------|---------|-----|-------|----------|----------| 			    
	| blue-collar   | 0.18  | married  | 0.57    | secondary | 0.49      | cellular  | 0.72    | may | 0.25  | success  | 0.10     |
    | management    | 0.22  | single   | 0.32    | tertiary  | 0.33      | telephone | 0.07    | jul | 0.13  | failure  | 0.11     |
    | technician    | 0.17  | divorced | 0.18    | primary   | 0.13      | unknown   | 0.21    | aug | 0.13  | other    | 0.05     |
    | admin.        | 0.12  |          |         | unknown   | 0.04      |           |         | jun | 0.11  | unknown  | 0.74     |
    | services      | 0.08  |          |         |           |           |           |         | nov | 0.09  |          |          |
    | retired       | 0.07  |          |         |           |           |           |         | apr | 0.08  |          |          |
    | self-employed | 0.04  |          |         |           |           |           |         | feb | 0.07  |          |          |
    | entrepreneur  | 0.04  |          |         |           |           |           |         | jan | 0.03  |          |          |
    | unemployed    | 0.03  |          |         |           |           |           |         | oct | 0.04  |          |          |
    | housemaid     | 0.02  |          |         |           |           |           |         | sep | 0.03  |          |          |
    | student       | 0.03  |          |         |           |           |           |         | mar | 0.03  |          |          |
    | unknown       | 0.01  |          |         |           |           |           |         | dec | 0.01  |          |          |

<div align="center">

Table 9. Percentage of the Each Data Type in Categorical Data for Undersampling Data
</div>
<!-- blank line --> 

And some categorical data versus output:
	
    |output | job   | marital | education | default | housing | loan | contact | month | poutcome | 
    |-------|-------|---------|-----------|---------|---------|------|---------|-------|----------| 			
    | yes   | 0.13  | 0.12    | 0.12      | 0.09    | 0.12    | 0.10 | 0.11    | 0.24  | 0.26     |     
	| no    | 0.87  | 0.88    | 0.88      | 0.91    | 0.88    | 0.90 | 0.89    | 0.76  | 0.74     |

<div align="center">

Table 10. Percentage of the Categorical Data versus Output for Undersampling Data
</div>
<!-- blank line --> 
                                                                             
The percentage of all binary categorical data type contents percentage:
                                                                             
    |     | default | housing | loan |
    |-----|---------|---------|------|
    | yes | 0.02    | 0.56    | 0.16 | 
    | no  | 0.98    | 0.44    | 0.84 |
                                                                             
<div align="center">

Table 11. Percentage of the Positive/ Negative Sampling Categorical Data for Undersampling Data
</div>
<!-- blank line --> 
															 
Again, we check the missing data, the unknown pecentage of the job and education are above 0 and under 0.1. The marital and contact are above 0.1. 
The poutcome is close to 0.74, we will remove this data type because the pecentage is too high.

#### Feature Engineering from Basic Features
Before establing the model, we want to convert the variables to the features. There are some tips:
- Numerical Data:
  - From the correlation map, there is no correlation of the variables.
  - Comparing the normalization and the standarization variables as the features.
- Nominal Categorical Data: 
  - We use the one-hot encoder technology for the variables, such as the job, the marital, the education, the contact and the month.
  - Using the KNN to imputated some missing value, such as the job, the education, the contact and the poutcome.
  - Remove the poutcome due to the high missing percentage.
- Binary Categorical Data:
  - For binary variable, mapping the yes/no to the digital binary.
<!-- blank line -->  
We show the all features name after using one-hot encoding for the categorical data type:
                                                                             
    | job | marital | education | default | housing | loan | contact | month | poutcome |
    |-----|---------|-----------|---------|---------|------|---------|-------|----------|
	| x0  | x1      | x2        | x3      | x4      | x5   | x6      | x7    | x8       |
															
<div align="center">

Table 12. One-Hot Encoding Features for the Nominal Categorical Data Conversion
</div>
<!-- blank line --> 

On the other hand, ROC curve used to rank features in importance order, which gives a visual way to rank features performances. This technique is most suitable for binary classification tasks.
We show the ROC curve as following figures and display the AUC obove the 0.5:
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_nor_ohe_bi_features_ranking_roc.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 23. ROC Curve for Numerical Data Normalization
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_std_ohe_bi_features_ranking_roc.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 24. ROC Curve for Numerical Data Standarization
</div>  
<!-- blank line --> 

From above figures, we find that:
 - age (normaliztion, AUC = 0.668) > day (normaliztion, AUC = 0.653) > campaign (normaliztion, AUC = 0.613)> pday (normaliztion, AUC = 0.6)
 - duration (standardization, AUC = 0.805) > previous (standardization, AUC = 0.6)
 																				
#### Principle Component Analysis, PCA
In this section, we use the PCA to analyze the features after feature eingeering. 
We compare the normalization and the standardization numerical data after using PCA:
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_nor_ohe_bi_percentage_explained_variance.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 22. Percentage Explained Variance for Undersampling Data (Normalization)
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_std_ohe_bi_percentage_explained_variance.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 23. Percentage Explained Variance for Undersampling Data (Standarization)
</div>  
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_nor_ohe_bi_eigen_vector.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 24. Eigenvectors for Undersampling Data (Normalization)
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/db_imb_std_ohe_bi_eigen_vector.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 25. Eigenvectors for Undersampling Data (Standarization)
</div>
<!-- blank line --> 

Above figures, we list first 10 eigen vectors in the figures. We organize some tip:
- For numerical data normalization:
  - About first 15 eigenvalues reach the 80% variance importance (Figure 22.).
  - The first eigenvector: (Figure 24.)
    - The education, the job and the month are correlated.
	- Maybe it represents the finance resource.
  - The second eigenvector: (Figure 24.)
    - The types of the martial are highly correlated.
  - The third eigenvector: (Figure 24.)	
    -  The education, the job, the month and the poutcome are correlated.
- For numerical data standarization: 
  - About first 10 eigenvalues reach the 80% variance importance (Figure 23.).
  - The first eigenvector shows:(Figure 25.)
    - The campaign, the pdays and the previous are correlated.
  - The second eigenvector: (Figure 25.)
    - The age and the martial are correlated. 
  - The third eigenvector: (Figure 25.)	
    -  The balance, the duration and the campaign are correlated. 

#### Model Established
According to the statistics theory, there are at least 5000 data to established the prediction model; for this case, there are over 5000 postive samples after undersampling.
Furthermore, we extract the 7:3 training-testing/ validation data sets from the samling data. According to the machine learning theory, we select the some models to established the prediction model because there is the different VC dimension for the each model.
We use the nested cross-validation grid search included model established and parameters tunning. 
For this project, we choose the some models as the candidated models, including the Bagging, AdaBoostClassifier, DecisionTreeClassifier, ExtraTreesClassifier, RandomForestClassifier, XGBClassifier​, KNeighborsClassifier​.

<!-- blank line -->
Using the training-testing data set to execute the nested cross-validation grid search, we could evaluate the model from the learning curve as following figures:
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/nor_ohe_bi_cv_curve.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 26. Learning Curve for Numerical Data Normalization
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/std_ohe_bi_cv_curve.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 27. Learning Curve for for Numerical Data Standarization
</div>    
<!-- blank line --> 

And we organize the tables:

     | Classifier Name        | Classifier Train Accuracy Mean | Classifier Test Accuracy Mean | Classifier Test Accuracy 3*STD | Classifier Time |
     | ---------------------- | ------------------------------ | ----------------------------- | ------------------------------ | --------------- |
     | XGBClassifier          | 0.93                           | 0.85                          | 0.03                           | 910.82          |
     | RandomForestClassifier | 0.97                           | 0.84                          | 0.03                           | 29.46           |
     | BaggingClassifier      | 0.98                           | 0.83                          | 0.03                           | 266.78          |
     | AdaBoostClassifier     | 0.84                           | 0.83                          | 0.04                           | 176.15          |
     | ExtraTreesClassifier   | 0.92                           | 0.81                          | 0.06                           | 14.22           |
     | DecisionTreeClassifier | 0.87                           | 0.78                          | 0.07                           | 3.23            |
     | KNeighborsClassifier   | 0.97                           | 0.67                          | 0.05                           | 33.69           |

<div align="center">

Table 13. Nested Cross-Validation Grid Search For Numerical Data Normalization
</div>

     | Classifier Name        | Classifier Train Accuracy Mean | Classifier Test Accuracy Mean | Classifier Test Accuracy 3*STD | Classifier Time |
     | ---------------------- | ------------------------------ | ----------------------------- | ------------------------------ | --------------- |
     | XGBClassifier          | 0.92                           | 0.86                          | 0.03                           | 815.74          |
     | RandomForestClassifier | 0.96                           | 0.85                          | 0.03                           | 20.29           |
     | BaggingClassifier      | 0.97                           | 0.85                          | 0.03                           | 250.83          |
     | AdaBoostClassifier     | 0.84                           | 0.84                          | 0.04                           | 147.85          |
     | ExtraTreesClassifier   | 0.93                           | 0.82                          | 0.03                           | 13.40           |
     | DecisionTreeClassifier | 0.89                           | 0.81                          | 0.03                           | 2.08            |
     | KNeighborsClassifier   | 1.00                           | 0.80                          | 0.03                           | 38.61           |

<div align="center">

Table 14. Nested Cross-Validation Grid Search For Numerical Data Standarization
</div>

Furthermore, we would use the validation data as the model input and produced the output to plot the ROC curve plot:
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/nor_ohe_bi_roc.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 28. ROC Curve for Numerical Data Normalization 
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/std_ohe_bi_roc.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 29. ROC Curve for for Numerical Data Standarization 
</div>          
<!-- blank line --> 

From above figures and tables, we organize some tips for these models:
- Some model degree of the fitting for numerical data normalization (Table 13.-14., Figure 26.-27.): 
  - AdaBoostClassifier fitting well, other models overfitting due to seldom data (Figure 26.-27.).
  - XGBClassifier is the best model of the nested cross-validation stage (Table 13.-14.).
- Some model degree of the fitting for numerical data standarization (Table 13.-14., Figure 26.-27.):
  - AdaBoostClassifier fitting well; secondly, XGBClassifier (Figure 26.-27.).
  - XGBClassifier is the best model of the nested cross-validation stage (Table 13.-14.).
- From ROC curve for numerical data normalization (Figure 28.-29.):
  - XGBClassifier (0.92, BaggingClassifier (0.91), AdaBoostClassifier (0.90) and RandomForestClassifier (0.90) are the best model from the AUC.
- From ROC curve for numerical data standarization (Figure 28.-29.):
  - XGBClassifier (0.94), BaggingClassifier (0.92), RandomForestClassifier (0.91), AdaBoostClassifier (0.91), and  ExtraTreesClassifier (0.91) are the best model from the AUC.
- We would choose the best model from the learning curve and the roc curve (Table 11.-Table 12, Figure 26.-29.). 

From above description, we understand the best model are XGBClassifier for Numerical Data Standarization; moreover, we summerize these model report:

     |              | precision          | recall             | f1-score           | support            |
     | ------------ | ------------------ | ------------------ | ------------------ | ------------------ |
     | 0            | 0.88               | 0.85               | 0.86               | 1194.0             |
     | 1            | 0.86               | 0.89               | 0.87               | 1239.0             |
     | accuracy     | 0.87               | 0.87               | 0.87               | 0.87               |
     | macro avg    | 0.87               | 0.87               | 0.87               | 2433.0             |
     | weighted avg | 0.87               | 0.87               | 0.87               | 2433.0             |

</td></tr></table> 
<div align="center">

Table 15. Best Model Performace Summary Report for XGBClassifier Model 
</div> 

In this project, we focus on the positive prediciton system established.

#### Some Information from Tree Based Models:
We would get some information about the features importance by the some tree based models.
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/nor_ohe_bi_feature_importance.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 30. Feature Importance for Numerical Data Normalization
</div>
<!-- blank line --> 
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/std_ohe_bi_feature_importance.svg" align="center" alt="Project icon">
</td></tr></table> 
<div align="center">

Figure 31. Feature Importance for Numerical Data Standarization
</div>
<!-- blank line --> 

From above figures, we understand that:
- For Tree based models for numerical data normalization (AdaBoostClassifier, DecisionTreeClassifier, RandomForestClassifier, ExtraTreesClassifier​, XGBClassifier) (Figure 30.-31.):
  - There are high test score from the first to third ranking: XGBClassifier, RandomForestClassifier, AdaBoostClassifier
  - housing is the most important factor in the XGBClassifier. Secondly, others are contact, pdays, month, duration.
  - The RandomForestClassifier Features Importance: duration > pdays > age > balance > campaign > day > poutcome > contact > housing > previous
  - pday is the most important factor in the DecisionTreeClassifier. Secondly, others are duration, poutcome, campaign, housing, age, balance, day.
- For Tree based models for numerical data normalization (AdaBoostClassifier, DecisionTreeClassifier, RandomForestClassifier, ExtraTreesClassifier, XGBClassifier​) (Figure 30.-31.):
  - There are high test score from the first to third ranking: XGBClassifier, RandomForestClassifier, AdaBoostClassifier
  - The RandomForestClassifier Features Importance: housing > duration > contact > month > job
  - The ExtraTreesClassifier Features Importance: duration
  - The DecisionTreeClassifier Features Importance: duration and poutcome

### Analysis Conclusion
We organize some tips from above analysis:
- There are three types for this marketing analysis: personal background information, client-bank information and campaign information. Personal income of personal background (including income degree: job, income time: month), personal balance (client-bank information)the degree of the personal product campaign accept (campaign information, such as the duration) are the important factors (From Basic Profile, Features Importance & PCA).
- Model prediction performance for the numerical data standarization is better than normalization (From model established).
	 
## Part III. Quick Start
In this project, we use the preprocessing imbalance data sets to tran/test the model, and validate the model. 
We show the several functions of the project as following detail description:

### Project Lib Architecture
In this project, we need three librarys:
- bankmarketing: Bank Marketing Analysis Lib
  - bankmarketing_main: main file (python file)
  - bankmarketing_preprocessing: some functions are about data sets preprocessing (python file)   
  - bankmarketing_model_creator: some functions are about training/ testing/ validation (python file)

- CommonLib: Common Utility Tool Lib (Note: Using this library, it is important to setup for some cython files!)
  - CommonUtility: Common utility tool (python file)
 
- MachineLearningLib: Various Machine Learning Model
  - Exploratory Data Analysis:  some functions about Exploratory Data Analysis (python file)
  - Learning Analysis: some features analysis by using machine learning (python file)
  - Model Creator Helper: Some training/ testing/ validation functions (python file)
  - Model Evaluation: Some model evaluation functions (python file)
  - Preprocessing Utility: Data Preprocessing (python file)
  - Sampling Lib: Some sampling technique (python file)
  - MultiModelTool: Some models organization (python file)

### Model Established Pipeline
In this project, there are six steps for bank marketing product prediction model established, five steps for kaggle competition datasets recognizer established.
<!-- blank line --> 

#### Model System Prediction for Titanic Problem
- exploratory_data_analysis_stage: Excute the EDA for variables in train and test file.
- imbalance_data_preparation: Model established for the imbalance data preparation.
- imbanlance_data_features_selection_engineering: features selections through the feature engineering and analysis.
- imbanlance_data_learning_analysis: Using the learning analysis to anlyze the features.
- training_stage: Using imblanlance data to establised the model by training.
- model_organization: Factors of the models analysis.

### Operation Mode
#### IDE mode:
Excute the bankmarketing_main.py in different IDE.
#### CMD mode:
- Set cmd_config.txt file for model prediction system established.
- Excute command line in windows cmd, such as the enable_hw.bat file.

## Part IV. Other Records
### Some Notes
If you have any questions or suggestions, please let me know, thank you. Enjoy it!

### History Notes
     DataVer    Author      Note
     20190824   C.Y. Peng   First Release(20190824-R 1.0.0)
     20191101   C.Y. Peng   ML Lib New Function Included
     20191107   C.Y. Peng   Using Model Helper
     20191111   C.Y. Peng   Features Importance/ ROC
     20191202   C.Y. Peng   add the CMD mode, IDE mode
     20191204   C.Y. Peng   add the model_organization
     20200112   C.Y. Peng   Second Release(20200112-R 1.0.0)
	 
## Part IV. Reference
### Some Official/Papers/Books Reference
- [1] S. Moro, P. Cortez and P. Rita, "A Data-Driven Approach to Predict the Success of Bank Telemarketing. Decision Support Systems, " Moro et al., Elsevier, June 2014​
- [2] Vitor V. Lopes, and Jose´ C. Menezes, "Inferential sensor design in the presence of missing data: a case study, " Moro et al., Elsevier, 2005