'''
@Author: CY Peng
@Date: 2019-08-25
@LastEditTime: 2019-11-11

DataVer  - Author - Note
2019/08/24    CY    First Release(20190824-R 1.0.0)
2019/11/1     CY    ML Lib New Function Included
2019/11/7     CY    Using Model Helper
2019/11/11    CY    Features Importance/ ROC
2019/12/2     CY    add the CMD mode, IDE mode
2019/12/4     CY    add the model_organization
2020/01/12    CY    Second Release(20200112-R 1.0.0)

Ref.
https://developers.google.com/machine-learning/data-prep/construct/sampling-splitting/imbalanced-data, Imbalance Data Processing
https://www.kaggle.com/yufengsui/machine-learning-project-bank-marketing-analytics/notebook, kaggle player
'''
import sys
import os
import re
import bankmarketing_preprocessing as bk_db
import bankmarketing_model_creator as bk_model
from configparser import ConfigParser
import bclfMultiModelTool as MMT

def exploratory_data_analysis_stage(database_filename = 'db', database_folder = "database"):
    bk_db.EDABasicProfile(database_filename, database_folder)#, "fundamental_analysis")
    bk_db.EDABasicStatistics(database_filename, database_folder)
    bk_db.EDABasicStatisticsLabeledCorr(database_filename, database_folder)

def imbalance_data_preparation(database_filename = 'db', database_folder = "database"):
    bk_db.ImbalanceRawDataPrepraption(database_filename, database_folder)

def imbanlance_data_features_selection_engineering(database_filename = 'db_imb', database_folder = "database"):
    bk_db.FeaturesEngineering(database_filename, database_folder)

def imbanlance_data_learning_analysis(database_filename = "db", database_folder = "database"):
    bk_db.EDAComponentsAnalysis(database_filename, database_folder)
    bk_db.EDADTDumpROC(database_filename, database_folder)

def training_stage(raw_data_file = 'db_imb',
                   database_folder = "database",
                   train_file_tail_tag = None,
                   features_engineering_model_path = os.path.join(".","EDA_output","db_imb"),
                   model_config_filename='models_config_test'):
    bk_model.nested_training_stage(raw_data_file,
                                   database_folder,
                                   train_file_tail_tag = train_file_tail_tag,
                                   features_engineering_model_path = features_engineering_model_path,
                                   model_config_filename =model_config_filename)

def model_organization(model_path = ".\ML_output", **kwargs):
    MMT.model_organization(model_path, **kwargs)


def main():
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    print('Execution Scope {}'.format(config['Execution']['scope']))
    print('Parameters Setting {}'.format(kewgs))


    if 'exploratory_data_analysis_stage'== config['Execution']['scope']:
        exploratory_data_analysis_stage(**kewgs)
    if 'imbalance_data_preparation'== config['Execution']['scope']:
        imbalance_data_preparation(**kewgs)
    if 'imbalance_data_exploratory_data_analysis_stage'== config['Execution']['scope']:
        exploratory_data_analysis_stage(**kewgs)
    if 'imbanlance_data_features_selection_engineering'== config['Execution']['scope']:
        imbanlance_data_features_selection_engineering(**kewgs)
    if 'imb_nor_ohe_bi_imbanlance_data_learning_analysis'== config['Execution']['scope']:
        imbanlance_data_learning_analysis(**kewgs)
    if 'imb_std_ohe_bi_imbanlance_data_learning_analysis' == config['Execution']['scope']:
        imbanlance_data_learning_analysis(**kewgs)
    if 'imb_nor_ohe_bi_training_stage' == config['Execution']['scope']:
        training_stage(**kewgs)
    if 'imb_std_ohe_bi_training_stage' == config['Execution']['scope']:
        training_stage(**kewgs)
    if 'model_organization'== config['Execution']['scope']:
        model_organization(**kewgs)

    print('Completed!')

def ide_mode():
    exploratory_data_analysis_stage(database_filename = 'db', database_folder = "database")
    imbalance_data_preparation(database_filename='db', database_folder="database")
    exploratory_data_analysis_stage(database_filename="db_imb", database_folder="database")
    imbanlance_data_features_selection_engineering(database_filename='db_imb', database_folder="database")
    imbanlance_data_learning_analysis(database_filename="db_imb_nor_ohe", database_folder="database")
    imbanlance_data_learning_analysis(database_filename="db_imb_std_ohe", database_folder="database")
    training_stage(raw_data_file = 'db',
                   database_folder = "database",
                   train_file_tail_tag = "_nor_ohe_bi",
                   features_engineering_model_path = os.path.join(".","EDA_output","db_imb"),
                   model_config_filename='models_config_test')
    training_stage(raw_data_file = 'db',
                   database_folder = "database",
                   train_file_tail_tag = "_std_ohe_bi",
                   features_engineering_model_path = os.path.join(".","EDA_output","db_imb"),
                   model_config_filename='models_config_test')
    model_organization(model_path = ".\ML_output")
    
def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__ == "__main__":
    main()